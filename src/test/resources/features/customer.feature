@Customer
Feature: Customer feature

  Scenario: Get customers list
    When I call list
    Then I receive the list with all customers
      |Ivan|Ivan.PerezBrea@cognizant.com|
      |Pepe|Pepe@ing.es|


  Scenario: Search for customer
    When I search for a specific customer by name
    Then I receive the expected customer
      |Ivan|Ivan.PerezBrea@cognizant.com|

  Scenario:
    When I create new customer
    Then I receive "201" response