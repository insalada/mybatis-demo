package es.ing.demo.stepdefs;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import es.ing.demo.entity.Customer;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.spring.CucumberContextConfiguration;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

@SpringBootTest
@AutoConfigureMockMvc
@CucumberContextConfiguration
public class CustomerStepDefinitions {

    private static ObjectMapper mapper = new ObjectMapper();

    private static String CUSTOMER_LIST_ENDPOINT = "/list";
    private static String CUSTOMER_FIND_ENDPOINT = "/find";
    private static String CUSTOMER_CREATE_ENDPOINT = "/add";
    private static String EXPECTED_NAME_FILTER = "?name=Ivan";
    private static String NEW_CUSTOMER_NAME = "newCustomer";
    private static String NEW_CUSTOMER_EMAIL = "newEmail@ing.es";
    private static int NAME_COLUMN = 0;
    private static int EMAIL_COLUMN = 1;

    @Autowired
    private MockMvc mockMvc;

    private List<Customer> actualCustomers;
    private int actualStatus;

    @When("I call list")
    public void call_list() throws Exception {
        MvcResult result = mockMvc.perform(
            get(CUSTOMER_LIST_ENDPOINT))
            .andExpect(status().isOk())
            .andReturn();

        actualCustomers = readCustomers(result.getResponse().getContentAsString());
    }

    @Then("I receive the list with all customers")
    public void i_receive_customers_list(DataTable dataTable) {
        List<Customer> expected = dataTable.asLists().stream()
            .map(this::toCustomer)
            .collect(Collectors.toList());

        assertEquals(expected, actualCustomers);
    }

    @When("I search for a specific customer by name")
    public void find_customer_by_name() throws Exception {
        StringBuilder url = new StringBuilder(CUSTOMER_FIND_ENDPOINT).append(EXPECTED_NAME_FILTER);
        MvcResult result = mockMvc.perform(
            get(url.toString()))
            .andExpect(status().isOk())
            .andReturn();

        actualCustomers = readCustomers(result.getResponse().getContentAsString());
    }

    @Then("I receive the expected customer")
    public void i_receive_expected_customer(DataTable dataTable) {
        List<Customer> expected = dataTable.asLists().stream()
            .map(this::toCustomer)
            .collect(Collectors.toList());

        assertEquals(expected, actualCustomers);
    }

    @When("I create new customer")
    public void create_new_customer() throws Exception {
        Customer customer = new Customer(NEW_CUSTOMER_NAME, NEW_CUSTOMER_EMAIL);
        actualStatus = mockMvc.perform(post(CUSTOMER_CREATE_ENDPOINT)
            .content(mapper.writeValueAsString(customer))
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON))
            .andReturn().getResponse().getStatus();

    }

    @Then("I receive {expectedStatus} response")
    public void receive_ok_response(int expectedStatus) {
        assertEquals(expectedStatus, actualStatus);
    }

    private List<Customer> readCustomers(String content) throws Exception {
        return mapper.readValue(content, new TypeReference<List<Customer>>() {
        });
    }

    private Customer toCustomer(List<String> columns) {
        return new Customer(columns.get(NAME_COLUMN), columns.get(EMAIL_COLUMN));
    }

}
