package es.ing.demo.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import es.ing.demo.entity.Customer;
import es.ing.demo.mapper.CustomerMapper;
import java.util.List;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.Transactional;

@SpringBootTest
@AutoConfigureMockMvc
public class CustomerControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private CustomerMapper customerMapper;

    private static ObjectMapper mapper;
    private static String CUSTOMER_LIST_ENDPOINT = "/list";
    private static String CUSTOMER_FIND_ENDPOINT = "/find";
    private static String CUSTOMER_CREATE_ENDPOINT = "/add";
    private static String EXPECTED_NAME_FILTER = "?name=Ivan";
    private static String NEW_CUSTOMER_NAME = "newCustomer";
    private static String NEW_CUSTOMER_EMAIL = "newEmail@ing.es";

    @BeforeAll
    public static void setUp() {
        mapper = new ObjectMapper();
    }

    @Test
    public void shouldReturnAllCustomers() throws Exception {
        MvcResult result = mockMvc.perform(
            get(CUSTOMER_LIST_ENDPOINT))
            .andExpect(status().isOk())
            .andReturn();

        List<Customer> actual = mapper.readValue(
            result.getResponse().getContentAsString(), new TypeReference<List<Customer>>() {});
        assertFalse(actual.isEmpty(), "list must not be empty");
    }

    @Test
    public void shouldReturnDataFilteredOut() throws Exception {
        StringBuilder url = new StringBuilder(CUSTOMER_FIND_ENDPOINT).append(EXPECTED_NAME_FILTER);
        MvcResult result = mockMvc.perform(
            get(url.toString()))
            .andExpect(status().isOk())
            .andReturn();

        Customer expected = new Customer("Ivan", "Ivan.PerezBrea@cognizant.com");
        Customer actual = mapper.readValue(result.getResponse().getContentAsString(),
            new TypeReference<Customer>() {});
        assertNotNull(actual, "filtered customer must not be null");
        assertEquals(expected, actual, "Customer must match");
    }

    @Test
    @Transactional
    public void shouldCreateCustomer() throws Exception {
        Customer customer = new Customer(NEW_CUSTOMER_NAME, NEW_CUSTOMER_EMAIL);
        mockMvc.perform(post(CUSTOMER_CREATE_ENDPOINT)
            .content(mapper.writeValueAsString(customer))
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isCreated());

        Customer actual = customerMapper.findAll().stream()
            .filter(c -> NEW_CUSTOMER_NAME.equals(c.getName()))
            .findAny()
            .orElse(null);

        assertNotNull(actual, "Created user must not be null");
    }

}
