package es.ing.demo;

import io.cucumber.spring.CucumberContextConfiguration;
import org.springframework.boot.test.context.SpringBootTest;

@CucumberContextConfiguration
@SpringBootTest(classes = CucumberRunner.class)
public class CucumberSpringConfiguration {

//    private static Logger LOGGER = LoggerFactory.getLogger(CucumberSpringConfiguration.class);
//
//    @Before
//    public void setUp() {
//        LOGGER.info("-------------- Initilizating Cucumber Tests --------------");
//    }
}
