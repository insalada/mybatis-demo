package es.ing.demo.controller;

import es.ing.demo.entity.Customer;
import es.ing.demo.mapper.CustomerMapper;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CustomerController {

    private static Logger LOGGER = LoggerFactory.getLogger("CustomerController.class");

    @Autowired
    private CustomerMapper mapper;

    @GetMapping("/find")
    @ResponseBody
    public List<Customer> customer(@RequestParam String name) {
        return mapper.findByName(name);
    }

    @GetMapping("/list")
    @ResponseBody
    public List<Customer> list() {
        return mapper.findAll();
    }

    @PostMapping("/add")
    @ResponseBody
    public ResponseEntity add(@RequestBody Customer customer) throws Exception {
        LOGGER.debug("customer to insert: {}", customer);
        mapper.insert(customer);
        return new ResponseEntity(HttpStatus.CREATED);
    }

}
