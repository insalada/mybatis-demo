package es.ing.demo.mapper;

import es.ing.demo.entity.Customer;
import java.util.List;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface CustomerMapper {

    @Select("SELECT * FROM CUSTOMER WHERE name = #{name}")
    List<Customer> findByName(@Param("name") String name);

    @Select("SELECT * FROM customer")
    List<Customer> findAll();

    @Insert("INSERT INTO customer (name, email) VALUES (#{customer.name}, #{customer.email})")
    Integer insert(@Param("customer") Customer customer) throws Exception;

}
