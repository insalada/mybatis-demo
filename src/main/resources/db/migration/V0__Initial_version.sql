CREATE TABLE IF NOT EXISTS customer (

    id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name varchar(20),
    email varchar(50)

);


INSERT INTO customer (id, name, email) VALUES (1, 'Ivan', 'Ivan.PerezBrea@cognizant.com');
INSERT INTO customer (id, name, email) VALUES (2, 'Pepe', 'Pepe@ing.es');
